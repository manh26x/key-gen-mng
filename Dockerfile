FROM openjdk:8
ADD target/KeyGenMng-0.0.1-SNAPSHOT.jar KeyGenMng.jar
EXPOSE 9999
ENTRYPOINT ["java", "-jar", "KeyGenMng.jar"]
