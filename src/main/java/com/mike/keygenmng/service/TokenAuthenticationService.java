package com.mike.keygenmng.service;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.util.Date;

public class TokenAuthenticationService {

    static final long EXPIRATIONTIME = 30L*24L*60L*60L*1000L; // 1 month

    static final String SECRET = "ThisIsASecret";

    public static String addAuthentication(String username) {
        return Jwts.builder().setSubject(username)
                .setExpiration(new Date(System.currentTimeMillis() + EXPIRATIONTIME))
                .signWith(SignatureAlgorithm.HS512, SECRET).compact();
    }

    public boolean getExpiration(String token) {
        if (token != null) {
            // parse the token.
            return Jwts.parser().setSigningKey(SECRET).parseClaimsJws(token).getBody().getExpiration().after(new Date());
        }
        return false;
    }
}
