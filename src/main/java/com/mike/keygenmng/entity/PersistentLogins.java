package com.mike.keygenmng.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Data
public class PersistentLogins {
    private String username;
    @Id
    private String series;
    private String token;
    private String lastUsed;
}
