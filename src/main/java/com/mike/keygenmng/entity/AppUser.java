package com.mike.keygenmng.entity;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.text.SimpleDateFormat;
import java.util.Date;

@Data
@Entity
public class AppUser {

    @Id
    private String username;
    private String name;
    private String password;
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date expiredDate;
    @Column(length = 1000)
    private String token;
    public String getExpiredDate() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
        simpleDateFormat.applyPattern("dd/MM/yyyy");
        return simpleDateFormat.format(expiredDate);
    }

    public Date getExpired() {
        return expiredDate;
    }
}
