package com.mike.keygenmng.request.api;

import lombok.Data;

@Data
public class AuthenticationObj {
    private String username;
    private String password;
}
