package com.mike.keygenmng.request.form;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
@AllArgsConstructor
public class UserForm {
    private String username;
    private String name;
    private String password;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date expiredDate;
    private String usernameErr;
    private String passwordErr;
    private boolean created;

    public UserForm() {
        this.expiredDate = new Date();
        this.created = false;
    }
}
