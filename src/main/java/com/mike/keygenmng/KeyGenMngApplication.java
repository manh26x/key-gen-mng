package com.mike.keygenmng;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KeyGenMngApplication {

    public static void main(String[] args) {
        SpringApplication.run(KeyGenMngApplication.class, args);
    }

}
