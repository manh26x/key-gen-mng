package com.mike.keygenmng.controller;

import com.mike.keygenmng.entity.AppUser;
import com.mike.keygenmng.repository.UserRepository;
import com.mike.keygenmng.request.form.UserForm;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.security.Principal;

@Controller
public class MainController {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @PostMapping(value = { "/add-user" })
    public String savePerson(Model model, //
                             @ModelAttribute("userForm") UserForm userForm) {
        boolean isValid = true;
        if(userForm.getUsername() == null || userForm.getUsername().equals("")) {
            userForm.setUsernameErr("Trường bắt buộc");
            isValid = false;
        } else if(!userForm.getUsername().matches("\\w+")) {
            userForm.setUsernameErr("Tên tài khoản thuộc ký tự a-zA-Z0-9_");
            isValid = false;
        } else if(userRepository.findById(userForm.getUsername()).isPresent()) {
            userForm.setUsernameErr("Tên tài khoản đã được sử dụng");
            isValid = false;
        }
        if(userForm.getPassword() == null || userForm.getPassword().equals("")) {
            userForm.setPassword("Abc@1234");
        }
        if(userForm.getPassword().length() < 8) {
            userForm.setPasswordErr("Tối thiểu 8 ký tự");
            isValid = false;
        }
        userForm.setPassword(passwordEncoder.encode(userForm.getPassword()));
        if(!isValid) {
            model.addAttribute("showPopup", "show");
            model.addAttribute("userForm", userForm);
        } else {
            model.addAttribute("showPopup", "hide");

            AppUser user = new AppUser();
            BeanUtils.copyProperties(userForm, user);
            userRepository.save(user);
            model.addAttribute("successMessage", "Thêm mới người dùng '" +  user.getUsername() + "' thành công!");
            model.addAttribute("users", userRepository.findAll());
        }
        return "index";
    }

    @PostMapping(value = { "/update-user" })
    public String updatePerson(Model model, //
                             @ModelAttribute("userForm") UserForm userForm) {
        boolean isValid = true;
        if(userForm.getUsername() == null || userForm.getUsername().equals("")) {
            userForm.setUsernameErr("Trường bắt buộc");
            isValid = false;
        } else if(!userForm.getUsername().matches("\\w+")) {
            userForm.setUsernameErr("Tên tài khoản thuộc ký tự a-zA-Z0-9_");
            isValid = false;
        }

        if(userForm.getPassword().length() < 8) {
            userForm.setPasswordErr("Tối thiểu 8 ký tự");
            isValid = false;
        }
        if(!isValid) {
            model.addAttribute("showPopup", "show");
            model.addAttribute("userForm", userForm);
        } else {
            model.addAttribute("showPopup", "hide");

            AppUser user = userRepository.findById(userForm.getUsername()).get();
            user.setExpiredDate(userForm.getExpiredDate());
            user.setName(userForm.getName());
            if(userForm.getPassword() != null && !userForm.getPassword().equals("")) {
                user.setPassword(passwordEncoder.encode(userForm.getPassword()));
            }
            userRepository.save(user);
            model.addAttribute("successMessage", "Cập nhật người dùng '" +  user.getUsername() + "' thành công!");
            model.addAttribute("users", userRepository.findAll());
        }
        return "index";
    }

    @GetMapping(value = { "/admin", "/add-user", "/update-user"})
    public String homePage(Model model, @RequestParam(required = false) String search) {
        model.addAttribute("userForm", new UserForm());
        model.addAttribute("users", userRepository.searchUser(search));
        model.addAttribute("search", search);
        return "index.html";
    }


    @GetMapping(value = { "/"})
    public String redirectAdmin() {
        return "redirect:/user-info";
    }

    @GetMapping(value = { "/login" })
    public String login() {
        return "login.html";
    }

    @GetMapping(value = "/403")
    public String accessDenied(Model model, Principal principal) {

        if (principal != null) {
            User loginedUser = (User) ((Authentication) principal).getPrincipal();


            model.addAttribute("userInfo", loginedUser);

            String message = "Hi " + principal.getName() //
                    + "<br> You do not have permission to access this page!";
            model.addAttribute("message", message);

        }

        return "403Page";
    }
}
