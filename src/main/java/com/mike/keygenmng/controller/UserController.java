package com.mike.keygenmng.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class UserController {

    @GetMapping("user-info")
    public String getInfo(Model model) {
        return "userinfo";
    }
}
