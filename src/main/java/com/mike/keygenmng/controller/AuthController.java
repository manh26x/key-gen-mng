package com.mike.keygenmng.controller;

import com.mike.keygenmng.entity.AppUser;
import com.mike.keygenmng.repository.UserRepository;
import com.mike.keygenmng.request.api.AuthenticationObj;
import com.mike.keygenmng.request.api.HttpException;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Date;
import java.util.Optional;

@RestController
public class AuthController {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserRepository userRepository;
    static final String SECRET = "ThisIsASecret";

    @PostMapping(value ="token",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @ExceptionHandler(HttpMessageNotWritableException.class)
    public ResponseEntity<?> authentication(@RequestBody AuthenticationObj authenticationObj) {
        Optional<AppUser> appUser=  userRepository.findById(authenticationObj.getUsername());

        if(appUser.isPresent()) {
            Date now =  new Date();

            if(passwordEncoder.matches(authenticationObj.getPassword(), appUser.get().getPassword())) {
                if(now.after(appUser.get().getExpired())) {
                    throw  new ResponseStatusException(HttpStatus.EXPECTATION_FAILED, "Tài khoản hết hạn");
                }
                String token = Jwts.builder()
                        .setSubject(authenticationObj.getUsername())
                        .setHeaderParam("timeGen", System.currentTimeMillis())
                        .setExpiration(appUser.get().getExpired())
                        .signWith(SignatureAlgorithm.HS512, SECRET)
                        .compact();
                appUser.get().setToken(token);
                userRepository.save(appUser.get());
                return ResponseEntity.ok(token);
            }
           throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Sai mật khẩu");
        }
        throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Sai tài khoản");
    }

    @PostMapping(value="token/valid",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> authentication(@RequestBody String token) {
        Date expiredDate = null;
        String username = "";
        try {
            expiredDate = Jwts.parser().setSigningKey(SECRET).parseClaimsJws(token).getBody().getExpiration();
            username = Jwts.parser().setSigningKey(SECRET).parseClaimsJws(token).getBody().getSubject();
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Token Expired!");

        }

            Date now = new Date();
            if(expiredDate.after(now)) {
                Optional<AppUser> user = userRepository.findById(username);
                if(user.isPresent() && user.get().getExpired().after(now) && token.equals(user.get().getToken())) {
                    return ResponseEntity.ok(Boolean.TRUE);
                } else {
                    return ResponseEntity.ok(Boolean.FALSE);
                }
            }
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Token hết hạn");

    }


}
