package com.mike.keygenmng.repository;

import com.mike.keygenmng.entity.AppUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<AppUser, String> {
    @Query("select au from AppUser au where  au.name like %:search% or au.username like %:search%")
    List<AppUser> searchUser(String search);
}
